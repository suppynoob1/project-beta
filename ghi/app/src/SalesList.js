import React, { useEffect, useState } from 'react'

function SalesList(){
    const [sales, setSales] = useState([]);

    const fetchData = async() => {
        const url = 'http://localhost:8090/api/sales/'
        const response = await fetch(url)
        if(response.ok){
            const data=await response.json()
            setSales(data.sales)
        }
    }

    const deleteSale = async (id) => {
        const url = `http://localhost:8090/api/sales/${id}`
        const response = await fetch(url, {
            method: 'DELETE',
        })
        if(response.ok){
            setSales(sales.filter(sale => sale.id !== id))
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return(
    <>
    <h1>Sales</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>Salesperson Name</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {sales.map(sale =>
                    {
                        return(
                            <tr key={sale.id}>
                                <td>{sale.salesperson.employee_id}</td>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name} {sale.customer.last_name} </td>
                                <td>{sale.automobile.vin}</td>
                                <td>${sale.price}</td>
                                <td>
                                    <button onClick={() => deleteSale(sale.id)}>Delete</button>
                                </td>
                            </tr>
                        )
                    })}
            </tbody>
        </table>

    </>
    )
}

export default SalesList
