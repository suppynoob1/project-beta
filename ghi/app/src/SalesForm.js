import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'


function SalesForm(){
    const[automobiles, setAutomobiles] = useState([])
    const[salespeople, setSalesPeople] = useState([])
    const[customers, setCustomers] = useState([])
    const[newSalesperson, setNewSalesperson] = useState('')
    const[newCustomer, setNewCustomer] = useState('')
    const[vin, setVin] = useState('')
    const[price, setPrice] = useState('')

    const handleVinChange = (event) => {
        setVin((event.target.value))
    }
    const handleNewSalespersonChange = (event) => {
        setNewSalesperson(Number(event.target.value))
    }
    const handleNewCustomerChange = (event) => {
        setNewCustomer(Number(event.target.value))
    }
    const handlePriceChange = (event) => {
        setPrice(Number(event.target.value))
    }


    const navigate = useNavigate()
    const fetchAutomobiles = async () => {
        const response = await fetch("http://localhost:8090/api/automobileVOs/");
        if (response.ok) {
          const data = await response.json();
          const filtered = data.AutomobileVOs.filter((auto) =>!auto.sold)
          setAutomobiles(filtered);
        }
      };
      const fetchSalesPeople = async () => {
        const response = await fetch("http://localhost:8090/api/salespeople/");
        if (response.ok) {
          const data = await response.json();
          setSalesPeople(data.salesperson);
        }
      };
      const fetchCustomers = async () => {
        const response = await fetch("http://localhost:8090/api/customers/");
        if (response.ok) {
          const data = await response.json();
          setCustomers(data);
        }
      };

      const handleSubmit = async(event) =>{
        event.preventDefault()
        const data = {}
        data.automobile = vin
        data.salesperson = newSalesperson
        data.customer = newCustomer
        data.price = price

        const salesURL = 'http://localhost:8090/api/sales/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        };
        console.log('Data to be sent:', data);
        const response = await fetch(salesURL, fetchConfig)
        if(response.ok){
            setNewSalesperson('')
            setNewCustomer('')
            setVin('')
            setPrice('')
            navigate('/sales')
        }
    }

    useEffect(() => {
        fetchAutomobiles();
        fetchCustomers();
        fetchSalesPeople();
    }, []);

    return(
    <div className="row" style={{width:"50em"}}>
        <div className="offset-3">
            <div className="shadow p-4 mt-4">
                <h1>Create a new sale</h1>
                <form onSubmit={handleSubmit} id="create-new-sale">

                    <div className="mb-3">
                        <label htmlFor='VIN'> VIN</label>
                        <select onChange={handleVinChange} required name="vin" id="vin" className="form-select">
                            <option value={vin} >VIN Number...</option>
                            {automobiles?.map((a) => {
                                return (
                                    <option key={a.id} value={a.vin}>
                                        {a.vin}
                                    </option>
                                );
                                return null;
                            })}
                        </select>
                    </div>
                    <div className="mb-3">
                        <label htmlFor='salesperson'> Salesperson</label>
                        <select onChange={handleNewSalespersonChange} required name="salesperson" id="salesperson" className="form-select">
                            <option value={newSalesperson} >Salesperson...</option>
                            {salespeople?.map(sp =>{
                                return(
                                    <option key={sp.id} value={sp.id}>
                                        {sp.first_name} {sp.last_name}
                                    </option>
                                )
                            })}
                        </select>
                    </div>

                    <div className="mb-3">
                        <label htmlFor='customer'> Customer</label>
                        <select onChange={handleNewCustomerChange} required name="customer" id="customer" className="form-select">
                            <option value={newCustomer} >Customer...</option>
                            {customers?.map((c) => {
                                return (
                                    <option key={c.id} value={c.id}>
                                        {c.first_name} {c.last_name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>

                    <div className="form-floating mb-3">
                        <input value={price} onChange={handlePriceChange} placeholder="price" required type="number" name="price" id="price" className="form-control"/>
                        <label htmlFor="price"> Price </label>
                    </div>

                    <button className="btn btn-primary">Create</button>

                </form>
            </div>
        </div>
    </div>
    )
}

export default SalesForm
