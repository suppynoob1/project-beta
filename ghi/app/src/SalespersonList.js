import React, { useState, useEffect } from 'react'

function SalespersonList(){
    const [salesperson, setSalesperson] = useState([]);
    const fetchData = async() => {
        const url= 'http://localhost:8090/api/salespeople/'
        const response = await fetch(url);
        if(response.ok){
            const data = await response.json();
            setSalesperson(data.salesperson)
        }
    }

    const deleteSalesperson = async (id) => {
        const url = `http://localhost:8090/api/salesperson/${id}`
        const response = await fetch(url, {
            method: 'DELETE',
        })
        if(response.ok){
            setSalesperson(salesperson.filter(s => s.id !== id))
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return(
        <table className="table table-striped">
            <thead>
                <tr>
                   <th>Employee ID</th>
                   <th>first name</th>
                   <th>last name</th>
                   <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {salesperson.map(s =>
                   {
                    return (
                        <tr key={s.id}>
                            <td>{s.employee_id}</td>
                            <td>{s.first_name}</td>
                            <td>{s.last_name}</td>
                            <td>
                                <button onClick={() => deleteSalesperson(s.id)}>Delete</button>
                            </td>
                        </tr>
                    )
                   })}
            </tbody>
        </table>
    )
}

export default SalespersonList;
