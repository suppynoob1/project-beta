import React, { useEffect, useState } from "react";

function TechniciansList() {
    const [technicians, setTechnicians] = useState([])

    const getTechnicians = async() => {
        const url = 'http://localhost:8080/api/technicians/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setTechnicians(data.technicians)
        }
    }

    useEffect(() => {
        getTechnicians()
    },[]);

    const deleteTechnician = async(technician) => {
        const url = `http://localhost:8080/api/technicians/${technician.id}`
        const fetchConfig = {
            method: `DELETE`,
            headers: {
                'Content-type': 'application/json',
            },
        }
        try {
            const response = await fetch(url,fetchConfig)
            if (response.ok) {
                getTechnicians()
            }
        } catch(error) {
            console.log('error deleting')
        }
    }


    return (
        <>
            <table className='table table-striped'></table>
                <thead>
                    <tr>
                        <th> Employee ID</th>
                        <th> First Name</th>
                        <th> Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians?.map((technician) => {
                        return (
                            <tr key={technician.id}>
                                <td>{technician.employee_id}</td>
                                <td>{technician.first_name}</td>
                                <td>{technician.last_name}</td>
                                <td>
                                    <button onClick={() => deleteTechnician(technician)}>
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            <button onClick = {getTechnicians}> Refresh </button>
        </>
    )
}

export default TechniciansList;
