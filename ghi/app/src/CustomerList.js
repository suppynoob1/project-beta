import React, { useEffect, useState } from 'react';

function CustomerList() {
    const [customer, setCustomer] = useState([]);
    const fetchData = async () => {
        const customerUrl = 'http://localhost:8090/api/customers/'
        try {
            const response = await fetch(customerUrl);
            if (response.ok) {
                const data = await response.json();
                console.log('Data received from API:', data)
                setCustomer(data);
            } else {
                console.log('Response not OK');
            }
        } catch (error) {
            console.log('An error occurred: ', error);
        }
    }

    const deleteCustomer = async (id) => {
        const url = `http://localhost:8090/api/customers/${id}`
        const response = await fetch(url, {
            method: 'DELETE',
        })
        if(response.ok){
            setCustomer(customer.filter(c => c.id !== id))
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div>
            <h1>Customers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Address</th>
                        <th>Phone Number</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {customer.map(c => {
                        return (
                            <tr key={c.id}>
                                <td>{c.first_name}</td>
                                <td>{c.last_name}</td>
                                <td>{c.address}</td>
                                <td>{c.phone_number}</td>
                                <td>
                                    <button onClick={() => deleteCustomer(c.id)}>Delete</button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>

    )
}

export default CustomerList;
