import { NavLink } from 'react-router-dom';
import './Nav.css';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-orange">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <div className="dropdown ms-auto">
          <button className="btn btn-light dropdown-toggle me-2" type="button" id="inventoryDropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Inventory
            </button>
            <div className="dropdown-menu" aria-labelledby="inventoryDropdownMenuButton">
              <NavLink className="dropdown-item" aria-current="page" to="/manufacturers">Manufacturers</NavLink>
              <NavLink className="dropdown-item" aria-current="page" to="/manufacturers/new">Create a Manufacturer</NavLink>
              <div className='dropdown-divider'></div>
              <NavLink className="dropdown-item" aria-current="page" to="/models">Models</NavLink>
              <NavLink className="dropdown-item" aria-current="page" to="/models/new">Create a Model</NavLink>
              <div className='dropdown-divider'></div>
              <NavLink className="dropdown-item" aria-current="page" to="/automobiles">Automobiles</NavLink>
              <NavLink className="dropdown-item" aria-current="page" to="/automobiles/new">Create an Automobile</NavLink>
            </div>
            <button className="btn btn-light dropdown-toggle me-2" type="button" id="salesDropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Sales
            </button>
            <div className="dropdown-menu" aria-labelledby="salesDropdownMenuButton">
              <NavLink className="dropdown-item" aria-current="page" to="/salespeople">Salespeople</NavLink>
              <NavLink className="dropdown-item" aria-current="page" to="/salespeople/new">Add a Salesperson</NavLink>
              <div className='dropdown-divider'></div>
              <NavLink className="dropdown-item" aria-current="page" to="/customers/"> Customers</NavLink>
              <NavLink className="dropdown-item" aria-current="page" to="/customers/new"> Add a Customer</NavLink>
              <div className='dropdown-divider'></div>
              <NavLink className="dropdown-item" aria-current="page" to="/sales"> Sales</NavLink>
              <NavLink className="dropdown-item" aria-current="page" to="/sales/new"> Add a Sale</NavLink>
              <div className='dropdown-divider'></div>
              <NavLink className="dropdown-item" aria-current="page" to="/sales/history"> Sales History</NavLink>
            </div>
            <button className="btn btn-light dropdown-toggle me-2" type="button" id="serviceDropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Services
            </button>
            <div className="dropdown-menu" aria-labelledby="serviceDropdownMenuButton">
              <NavLink className="dropdown-item" aria-current="page" to="/technicians">Technicians</NavLink>
              <NavLink className="dropdown-item" aria-current="page" to="/technicians/new">Create a Technicians</NavLink>
              <div className='dropdown-divider'></div>
              <NavLink className="dropdown-item" aria-current="page" to="/appointments">Appointments</NavLink>
              <NavLink className="dropdown-item" aria-current="page" to="/appointments/new">Create an Appointment</NavLink>
              <div className='dropdown-divider'></div>
              <NavLink className="dropdown-item" aria-current="page" to="/appointments/history">Service History</NavLink>
            </div>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
