import React, { useEffect, useState } from "react";

function ListModels() {
    const [models,setModels] = useState([]);

    const getModels = async() => {
    const url = 'http://localhost:8100/api/models/'
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json()
        setModels(data.models)
    }
    }

    useEffect(() => {
        getModels();
    }, []);

    const deleteModel = async (model) => {
        const url = `http://localhost:8100/api/models/${model.id}`
        const fetchConfig = {
            method: 'DELETE',
            headers: {
                'Content-type': 'application/json',
            },
        }
        try{
            const response = await fetch(url, fetchConfig)
            if(response.ok) {
                getModels()
            }
        } catch(error) {
            console.log(`error deleting`)
        }
    }

    return (
        <>
            <div>
                <table className='table table-striped'>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Manufacturer</th>
                            <th>Picture</th>
                        </tr>
                    </thead>
                    <tbody>
                        {models.map((model) => {
                            return (
                                <tr key={model.id}>
                                    <td>{model.name}</td>
                                    <td>{model.manufacturer.name}</td>
                                    <td>
                                        <img
                                            src={model.picture_url}
                                            style={{ height: 100, width: 100 }}
                                        ></img>
                                    </td>

                                    <td>
                                        <button onClick={() => deleteModel(model)}>
                                            Delete
                                        </button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>

                <button onClick={getModels}> Refresh </button>
            </div>
        </>
    )
}

export default ListModels;
