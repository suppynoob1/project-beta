from django.urls import path
from .views import api_list_appointments, api_appointment_details,api_cancel_appointment,api_finished_appointment,api_list_technicians, api_technician_details, api_get_automobileVOs

urlpatterns = [
    path("appointments/", api_list_appointments, name='api_list_appointments'),
    path("appointments/<int:pk>/", api_appointment_details, name='api_appointment_details'),
    path("appointments/<int:pk>/cancel", api_cancel_appointment, name='api_cancel_appointment'),
    path("appointments/<int:pk>/finish", api_finished_appointment, name='api_finished_appointment'),

    path("technicians/", api_list_technicians, name='api_list_technicians'),
    path("technicians/<int:pk>/", api_technician_details, name='api_technician_details'),

    path ('automobileVOs/', api_get_automobileVOs, name='api_get_automobileVOs')
]
