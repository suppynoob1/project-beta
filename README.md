# CarCar

Team:
* Andrew Huang - Service microservice
* Paarth Joshi - Sales microservice


## Step by step of how to run project
- fork and clone the repository: https://gitlab.com/sjp19-public-resources/sjp-2022-april/project-beta
- Run the following commands in order

1. docker volume create beta-data
2. docker-compose build
3. docker-compose up

View the project in the browser using http://localhost:3000/

## Diagram of project - break down models and which services they live

![Project Beta Diagram](./projectbetadiagram.png)


## Explicitly defined urls and ports for each of the services

### Manufacturers:

 #### URL Endpoints:

|         Action                        |   Method    |             URL                               |
| ------------------------------------- | ----------- | --------------------------------------------- |
| List manufacturers                    | GET         |  http://localhost:8100/api/manufacturers/     |
| Create a manufacturers                | POST        |  http://localhost:8100/api/manufacturers/     |
| Get a specific manufacturers          | GET         |  http://localhost:8100/api/manufacturers/id/  |
| Update a specific manufacturers       | PUT         |  http://localhost:8100/api/manufacturers/id/  |
| DELETE a specific manufacturers       | DELETE      |  http://localhost:8100/api/manufacturers/id/  |



JSON body to send data:
- Create and Update a manufacturer (SEND THIS JSON BODY):

     manufacturers should be unique

```
{
  "name": "Chrysler"
}
```

- The return value of creating, viewing, updating a single manufacturer:

```
{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Chrysler"
}
```

- Getting a list of manufacturers return value:

```
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  ]
}
```

### Models

 #### URL Endpoints

|         Action                        |   Method    |             URL                               |
| ------------------------------------- | ----------- | --------------------------------------------- |
| List vehicle models                   | GET         |  http://localhost:8100/api/models/            |
| Create a vehicle model                | POST        |  http://localhost:8100/api/models/            |
| Get a specific vehicle model          | GET         |  http://localhost:8100/api/models/id/         |
| Update a specific vehicle model       | PUT         |  http://localhost:8100/api/models/id/         |
| DELETE a specific vehicle model       | DELETE      |  http://localhost:8100/api/models/id/         |


- Create and update a vehicle model (SEND THIS JSON BODY):
```
{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
  "manufacturer_id": 1
}
```

- Updating a vehicle model can take the name and/or picture URL:
```
{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
}
```

- Return value of creating or updating a vehicle model:


```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}
```

Getting a List of Vehicle Models Return Value:

```
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "image.yourpictureurl.com",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}
```

### Automobiles:

 #### URL Endpoints

|         Action                        |   Method    |             URL                                     |
| ------------------------------------- | ----------- | --------------------------------------------------- |
| List automobiles                      | GET         |  http://localhost:8100/api/automobiles/             |
| Create an automobile                  | POST        |  http://localhost:8100/api/automobiles/             |
| Get a specific automobile             | GET         |  http://localhost:8100/api/automobiles/vin/         |
| Update a specific automobile          | PUT         |  http://localhost:8100/api/automobiles/vin/         |
| DELETE a specific automobile          | DELETE      |  http://localhost:8100/api/automobiles/vin/         |

Create an automobile (SEND THIS JSON BODY):
- IMPORTANT: The identifiers for automobiles in this API are not integer ids. They are the Vehicle Identification Number (VIN) for the specific automobile. For example, you would use the URL

http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/

to get the details for the car with the VIN "1C3CC5FB2AN120174". The details for an automobile include its model and manufacturer.
- IMPORTANT: You cannot make two automobiles with the same vin

```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}
```

Return Value of Creating an Automobile:
```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "777",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "R8",
		"picture_url": "image.yourpictureurl.com",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Audi"
		}
	}
}
```

To get the details of a specific automobile, you can query by its VIN or ID:
example url: http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/
Return Value:
```
{
  "href": "/api/automobiles/1C3CC5FB2AN120174/",
  "id": 1,
  "color": "green",
  "year": 2011,
  "vin": "1C3CC5FB2AN120174",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "image.yourpictureurl.com",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  }
}
```

You can update the color and/or year of an automobile (SEND THIS JSON BODY):
```
{
  "color": "red",
  "year": 2012
}
```

Getting a list of Automobile Return Value:

```
{
  "autos": [
    {
      "href": "/api/automobiles/1C3CC5FB2AN120174/",
      "id": 1,
      "color": "yellow",
      "year": 2013,
      "vin": "1C3CC5FB2AN120174",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "Sebring",
        "picture_url": "image.yourpictureurl.com",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "Daimler-Chrysler"
        }
      }
    }
  ]
}
```


## Service microservice

### Service APIs

|         Action                        |   Method    |             URL                                         |
| ------------------------------------- | ----------- | ------------------------------------------------------- |
| List all technicians                  | GET         |  http://localhost:8080/api/technicians/                 |
| Create a technician                   | POST        |  http://localhost:8080/api/technicians/                 |
| Delete a specific technician          | DELETE      |  http://localhost:8080/api/technicians/:id/             |
| List appointments                     | GET         |  http://localhost:8080/api/appointments/                |
| Get specific appointment details      | GET         |  http://localhost:8080/api/appointments/:id/            |
| Create an appointment                 | POST        |  http://localhost:8080/api/appointments/                |
| Delete a specific appointment         | DELETE      |  http://localhost:8080/api/appointments/:id/            |
| Set appointment status to canceled    | PUT         |  http://localhost:8080/api/appointments/:id/cancel      |
| Set appointment status to finished    |  PUT        |  http://localhost:8080/api/apointments/:id/finish       |
| Get appointment history               |  GET        |  http://localhost:8080/api/apointments/                  |

### List Technicians
METHOD: 'GET'
URL: 'http://localhost:8080/api/technicians/'

  Lists the technicians:
  - does not require a request body when method is "GET" to show a list of all technicians. Making GET request to this end point simply returns the list of technicians with the first_name, last_name, employee_id and a unique id

Succesful Request Response:
```
 [
 	{
 		"first_name": "John",
 		"last_name": "Johns",
 		"employee_id": "jjohns",
 		"id": 1
 	}
 ]
```

### Create Technician
REQUEST METHOD: 'POST'
URL: 'http://localhost:8080/api/technicians/'

Creates a technician using the POST request method to the above URL.

example of a JSON body for creating a new technican:

```
 {
 	"first_name": "John",
 	"last_name": "Doe",
 	"employee_id": "jdoe"
 }
```

Successful Request Response:
```
 [
 {
 	"first_name": "John",
 	"last_name": "Doe",
 	"employee_id": "jdoe",
 	"id": 2
 },
 ]
```

### Delete Technician
REQUEST METHOD: 'DELETE'
URL: http://localhost:8080/api/technicians/:id/
- where "/:id/" is the technician id given by successful response request (ex: http://localhost:8080/api/technicians/2)
- To make a Delete request via Insomnia, simply select 'DELETE' and use the URL format shown above
- the JSON Response body will say deleted = true if the request was successful

### List Appointments
REQUEST METHOD: 'GET'
URL: http://localhost:8080/api/
- Listing service appointments is done by using the 'GET' method and using the URL shown in the line above

Successful Response to 'GET' request for appointments:
```
{
	"appointments": [
		{
			"id": 31,
			"date_time": "2023-06-10T11:57:00+00:00",
			"reason": "test",
			"status": "finished",
			"vin": "wdsa3213weaasa",
			"customer": "tesla",
			"technician": {
				"id": 9,
				"first_name": "you",
				"last_name": "sook",
				"employee_id": "sookyou"
			}
		},
		{
			"id": 32,
			"date_time": "2023-12-31T12:59:00+00:00",
			"reason": "test",
			"status": "finished",
			"vin": "dwasd",
			"customer": "tesla",
			"technician": {
				"id": 9,
				"first_name": "you",
				"last_name": "sook",
				"employee_id": "sookyou"
			}
		}
    ]
}
```

### Get Appointment Details
REQUEST METHOD: 'GET'
URL: http://localhost:8080/api/:id/
- Listing service appointment's details is done by using the 'GET' method and using the URL shown in the line above with id formatted like this example: http://localhost:8080/api/9/

Successful Response to 'GET' request for an appointment's details:
```
{
	"id": 1,
	"vin": "1222",
	"customer_name": "Barry",
	"time": "12:30:00",
	"date": "2021-07-14",
	"reason": "mah tires",
	"vip_status": false,
	"technician": "Liz"
}
```

### Delete a Specific Appointment
REQUEST METHOD: 'DELETE'
- similar to the other DELETE requests all we need to delete a specific appointment is using the DELETE request method and the url formatted in this way: http://localhost:8080/api/9/ where "9" is the id of the appointment that was returned.
- the JSON Response body will say deleted = true if the request was successful


 ### Set appointment status to canceled
 REQUEST METHOD: 'PUT'
 url: http://localhost:8080/api/appointments/:id/cancel
 - When accessing URL and submitting PUT request it changes the status to cancelled

### Set appointment status to finished
REQUEST METHOD:  'PUT'
URL: http://localhost:8080/api/apointments/:id/finish
-    When accessing URL and submitting PUT request it changes the status to finished




## Sales microservice


|         Action                        |   Method    |             URL                               |
| ------------------------------------- | ----------- | --------------------------------------------- |
| List salespeople                      | GET         |  http://localhost:8090/api/salespeople/       |
| Create a salesperson                  | POST        |  http://localhost:8090/api/salespeople/       |
| Delete a specific salesperson         | DELETE      |  http://localhost:8090/api/salespeople/:id/   |
| List customers                        | GET         |  http://localhost:8090/api/customers/         |
| Create a customer                     | POST        |  http://localhost:8090/api/customers/         |
| Delete a specific customer            | DELETE      |  http://localhost:8090/api/customers/:id/     |
| List sales                            | GET         |  http://localhost:8090/api/sales/             |
| Create a sale                         | POST        |  http://localhost:8090/api/sales/             |
| Delete a specific sale                |  DELETE     |  http://localhost:8090/api/sales/:id/         |
| List salesperson history              | GET         |  http://localhost:8090/api/sales              |


### Create a salesperson
Request Method: 'POST'
url: http://localhost:8090/api/salespeople/

```
{
	"first_name": "Jake",
	"last_name": "Jackson",
	"employee_id": "jjackson",
}
```

Response for a successful creation of a salesperson:
```
{
	"first_name": "Jake",
	"last_name": "Jackson",
	"employee_id": "jjackson",
	"id": 1
}
```


### Delete a salesperson
REQUEST METHOD: 'DELETE'
URL: http://localhost:8090/api/salespeople/:id/
- where "/:id/" is the technician id given by successful response request (ex: http://localhost:8080/api/salespeople/2)
- To make a Delete request via Insomnia, simply select 'DELETE' and use the URL format shown above
- the JSON Response body will say deleted = true if the request was successful



### List Salespeople:
Method: 'GET'
URL: http://localhost:8090/api/salespeople/

To list salespeople use GET and the URL above.

Successful REQUEST RESPONSE:
```
 {
 	"salespeople": [
 		{
 			"id": 1,
 			"first_name": "J",
 			"last_name": "Pie",
 			"employee_id": "jpie"
 		},
 		{
 			"id": 2,
 			"first_name": "Bob",
 			"last_name": "WeHadaBabyItsABoy",
 			"employee_id": "bboy"
 		}
 	]
 }
```











### List Customers:
Request Method: 'GET'
URL: http://localhost:8090/api/customers/




RESPONSE after successful request:
```

 {
 	"customers": [
 		{
 			"id": 1,
 			"first_name": "Do",
 			"last_name": "Bo",
 			"address": "1234 Sampl3 St",
 			"phone_number": "111111111"
 		},
 		{
 			"id": 2,
 			"first_name": "I",
 			"last_name": "  test  ",
 			"address": "a test",
 			"phone_number": "1234567890"
 		}
 	]
 }
```






### Create Customer:


Method: 'POST'
URL: http://localhost:8090/api/customers/


JSON body for insomnia request

```
 {
 	"first_name": "Peter",
 	"last_name": "Parker",
 	"address": " Nueva York ",
 	"phone_number": "9999999979"
 }
```

successful RESPONSE

```
 {
 	"id": 1,
 	"first_name": "Peter",
 	"last_name": "Parker",
 	"address": " Nueva York ",
 	"phone_number": "9999999979"
 }
```






### Delete a specific Customer:


Method: 'DELETE'
URL: http://localhost:8090/api/customers/:id

- no json body required. use returned id from response in place of ":id"


RESPONSE
```
 {
 	"message": "deleted"
 }
```
If customer is NOT present:

RESPONSE (status=400)
```
 {
 	"message": "Customer Object not present, Unable to delete"
 }

```





### List Sales:
Method: 'GET'
URL: http://localhost:8090/api/sales/

```
 {
 	"sales": [
 		{
 			"id": 5,
 			"automobile": {
 				"id": 5,
 				"vin": "ddasdsas21312",
 				"sold": true
 			},
 			"salesperson": {
 				"id": 2,
 				"first_name": "Dr",
 				"last_name": "Umar",
 				"employee_id": "dumar"
 			},
 			"customer": {
 				"id": 2,
 				"first_name": "John",
 				"last_name": "Johns",
 				"address": "Hell",
 				"phone_number": "1234567890"
 			},
 			"price": "1000"
 		}
 	]
 }
```






### Create a Sale:


Method: 'POST'
URL: http://localhost:8090/api/sales/



JSON body
```
 {
 	"automobile": "V1N",
 	"salesperson": "2",
 	"customer": "2",
 	"price": 200
 }
```

succesful RESPONSE


```
 {
 	"id": 1,
 	"automobile": {
 		"id": 2,
 		"vin": "V1N",
 		"sold": false
 	},
 	"salesperson": {
 		"id": 2,
 		"first_name": "R",
 		"last_name": "Gow",
 		"employee_id": "rgow"
 	},
 	"customer": {
 		"id": 2,
 		"first_name": "adqweqda",
 		"last_name": "earrrrrrrrrdqwdqwdrrr",
 		"address": "adafqdqad",
 		"phone_number": "1234"
 	},
 	"price": 200
 }
```






### Delete a Sale:


Method: 'DELETE'
URL: http://localhost:8090/api/sales/:id



success RESPONSE (status=200)
```
 {
 	"message": "deleted"
 }
```

If sales object is not present:

```
RESPONSE (status=400)

 {
 	"message": "Sale Object not present, Unable to delete"
 }
```

### Service and Salesperson History

List salesperson history
method: GET
URL:  http://localhost:8090/api/sales


List service history
method: GET
URL:  http://localhost:8080/api/appointments

- both servie and salesperson history use the filter function to filter for the specific sales/services based on the salesperson's ID and the VIN number of the automobiles for Sales and service respectively.


### Value Objects
- As shown in our diagram above sales and appointments both rely on AutomobileVO. the AutomobileVO for each microservice gets data about automobiles for recording a sale and creating an appointment using automobile data in the inventory microservice via the poller.
