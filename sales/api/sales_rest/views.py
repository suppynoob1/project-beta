from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Salesperson, Customer, AutomobileVO, Sale

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold", "id",]

class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "salesperson",
        "customer",
        "price",
        "automobile",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }



@require_http_methods(['GET', 'POST'])
def api_list_salespeople(request):
    if request.method == "GET":
        salesperson = Salesperson.objects.all()
        return JsonResponse(
            {"salesperson": salesperson},
            encoder = SalespersonEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalespersonEncoder,
            safe=False
        )



@require_http_methods(["GET", "DELETE"])
def api_show_salesperson(request, pk):
    if request.method == "GET":
        salesperson = Salesperson.objects.get(pk=pk)
        return JsonResponse(
            {"salesperson": salesperson},
            encoder=SalespersonEncoder
        )
    elif request.method == "DELETE":
        count, _ = Salesperson.objects.filter(pk=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Salesperson.objects.filter(pk=pk).update(**content)
        salesperson = Salesperson.objects.get(pk=pk)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


@require_http_methods(['GET', 'POST'])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        customers_data = list(customers.values('id', 'first_name', 'last_name', 'address', 'phone_number'))
        return JsonResponse(customers_data, encoder=CustomerEncoder, safe=False)
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            customer_data = {'id': customer.id, 'first_name': customer.first_name, 'last_name': customer.last_name, 'address': customer.address, 'phone_number': customer.phone_number}
            return JsonResponse(customer_data, encoder=CustomerEncoder)
        except Exception as e:
            return JsonResponse({"message": f"Could not create customer: {str(e)}"}, status=400)



@require_http_methods(["GET", "DELETE"])
def api_show_customer(request, id):
    try:
        if request.method == "GET":
            customer = Customer.objects.get(id=id)
            customer_data = {'id': customer.id, 'first_name': customer.first_name, 'last_name': customer.last_name, 'address': customer.address, 'phone_number': customer.phone_number}
            return JsonResponse(customer_data, encoder=CustomerEncoder)

        elif request.method == "DELETE":
            count, _ = Customer.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})

        else:
            content = json.loads(request.body)
            Customer.objects.filter(id=id).update(**content)
            customer = Customer.objects.get(id=id)
            customer_data = {'id': customer.id, 'first_name': customer.first_name, 'last_name': customer.last_name, 'address': customer.address, 'phone_number': customer.phone_number}
            return JsonResponse(customer_data, encoder=CustomerEncoder)

    except Customer.DoesNotExist:
        return JsonResponse({"message": "Customer not found"}, status=404)

    except Exception as e:
        return JsonResponse({"message": f"An error occurred: {str(e)}"}, status=400)




@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {'sales': sales},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            salesperson_id = content["salesperson"]
            customer_id = content["customer"]
            automobile_vin = content["automobile"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            customer = Customer.objects.get(id=customer_id)
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["salesperson"] = salesperson
            content["customer"] = customer
            content["automobile"] = automobile
            AutomobileVO.objects.filter(vin=automobile_vin).update(sold=True)
        except (Salesperson.DoesNotExist, Customer.DoesNotExist, AutomobileVO.DoesNotExist):
            return JsonResponse(
                {"message": "Invalid input"},
                status=400
            )

        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def api_show_sale(request, id):
    if request.method == "GET":
        sale = Sale.objects.get(id=id)
        return JsonResponse(
            {"sale": sale},
            encoder=SaleEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Sale.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Sale.objects.filter(id=id).update(**content)
        sale = Sale.objects.get(id=id)
        return JsonResponse(
            {"sale": sale},
            encoder=SaleEncoder,
            safe=False,
        )

@require_http_methods(["GET"])
def api_list_automobileVOs(request):
    if request.method == "GET":
        automobileVOs = AutomobileVO.objects.all()
        return JsonResponse({"AutomobileVOs": automobileVOs},AutomobileVOEncoder,safe=False,)
